## Overview

Find stale repos under a path or current path.

It will perform a fetch and check for untracked or (un)staged files.
Git only for now.
Written as sh script, since `find` is order of magnitude faster than a python walk.

## Usage

	# Current path.
	cd ~/projects && find_stale_repos

	# Under a path.
	find_stale_repos ~/projects

## License

find_stale_repos (C) 2016 Remik Ziemlinski 
GPLv3
